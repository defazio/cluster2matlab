% run clustermain
% get data
load('/Users/defazio/Documents/MATLAB/cluster2matlab/data/cluster test data.mat')
datain = LHInfusedC1RD;
%datain = set1C1RD; 
%datain = gnrh;
%datain = set1C1RD;
data = datain( ~isnan( datain ) ); % kill nan elements

n=size(data);
dx = 1;
xdata = dx * (1:n);
% get params
nPeak = 2; % npts to define an up
nNadir = 2; % npts to define a down
tScoreUp = 1; % min tscore for up
tScoreDn = 1; % min tscore for dpown
minPeak = 0; % min peak amplitude in data units
xHalfLife = 0; % not used 
xoutScore = 0; % not used
errType = 'Global SD'; % SQRT, Fixed, Global SD, Global SE, Local SD, Local SE
errVal = 0; % default error value, in data units
zero = 0; % value considered to be minimum data value, 'no activity'
xzeroTerminate = 0; % not used
xerrwn = 0; % not used yet, mechanism for user to provide error
xminnadir = 0; % not used
% run
[UPs, DNs, pulse, tUP, tDN] = clusterMain( data, nPeak, nNadir, ...
    tScoreUp, tScoreDn, minPeak, ...
    xHalfLife, xoutScore, errType, errVal, zero ); 
%... , xzeroTerminate, xerrwn, minnadir );
% display results
plotCluster(xdata,data,pulse,UPs,DNs, tUP, tDN);