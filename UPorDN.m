function [ w_output, w_tScore1, pMeans, nMeans ] = UPorDN( w, w_err, nPeak, nNadir, minT, zSign, dvmp)
% SUBROUTINE DNS
% w contains the data, a vector
% w_err contains a similar vector of error values for each data point

%ThreadSafe Function/s UPorDN(wn, wn_err, nPeak, nNadir, minT, zSign, dvmp)
%	string wn, wn_err
%	variable nPeak, nNadir,  minT, zSign, dvmp
% 	//nNadir - Size of base wave
% 	//nPeak - Size of test wave
% 	//minT - minimum T score for significant increase
% 	//zSign - specifies whether program determines ups or downs (1 = ups, -1 = downs)
% 	//dvmp - Minimum Data Value for Pulse

% MATLAB, copy vector info from data vector, w
    sz = size( w );
    %iLast = max( sz ) - nPeak + 1; % for IGOR, need to convert back to 1-based indices
    %iLast = max( sz ) - nPeak; %??? for IGOR, need to convert back to 1-based indices
    w_output = zeros( sz );
    w_base = zeros( sz );
    w_test = zeros( sz );
    w_tScore1 = zeros( sz );
    pMeans = zeros( sz );
    nMeans = zeros( sz );

    %iLast = max( sz ) - nPeak + 1; % for IGOR, need to convert back to 1-based indices
    iFirst = nNadir+1;
    iLast = max( sz ) - nPeak;% + 1; % matlab ??? for IGOR, need to convert back to 1-based indices
	for ipt = iFirst : iLast % loops over entire data vector, with edges to prevent over-run
		bMean = 0.0;
		tMean = 0.0;
        % baseline
		for i = 1 : nNadir % in FORTRAN ipt is NOT included in baseline
            j = ipt - i;
			%w_base(i) = w( j ); %w( ipt - nNadir + i );
			bMean =  bMean  + w(j); % w_base(i);
        end % LINE 1500
        bMean = bMean/nNadir;

        % are the points ipt to nPeak significantly greater/less than
        % baseline?
        for i = 1 : nPeak % in FORTRAN ipt is also included in test?!?
            j=ipt-1+i;
			%w_test(i) = w(ipt+i);
			tMean = tMean + w(j); % w_test(i);
        end % LINE 1600
		tMean = tMean/nPeak;

		sdev = w_err(ipt); %errw( i ) // error wave is created in ClusterMain

        % we broke out the FORTRAN code for scoring into a separate function
		tStat0 = mscore( ipt, nNadir, nPeak, sdev, w, w_err );

		w_tScore1(ipt) = tStat0;

		pMeans(ipt) = mean(w_test);
		nMeans(ipt) = mean(w_base);

    %// dvmp is minimum data value for a peak, i.e. minPeak = dvmp !!

		if zSign > 0
			if (tStat0 > minT) && (tMean > dvmp)  && (tMean > bMean) && (tStat0 ~= inf) 
				w_output(ipt) = 1;
            end
		else
			if (-1*tStat0 > minT ) && ( bMean > dvmp ) && ( bMean > tMean ) && (abs(tStat0) ~= inf) 
				w_output(ipt) = -1;
            end
        end

		%i+=1
	%while(i < iLast)
    end % LINE 9000 for loop over i
	%return wn_output

end


