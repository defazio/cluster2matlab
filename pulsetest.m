function w_pulse = pulseTest( w_ups, w_downs, nPeak, nNadir, ZeroTerminate, zero ) % , minPeak, minNadir )
%PULSETEST central component of cluster algo
%   Detailed explanation goes here
% porting from Igor which was ported from FORTRAN

    % Loop 1100/1102 of original fortran code. 
    % If first change is a down, a pulse is detected which has begun before data was recorded
    w_pulse = zeros( size( w_ups ) );
    npts = max(size( w_ups ));
    index = 1;
    down_index = 1;
    j=0;
    while true 
        if w_ups( index ) == 1   % && ( w[ index ] > tminpeak ) )
            j = 1;
        end
        if w_downs( index ) == -1 
            j = -1;
            down_index = index;
        end
        if j ~= 0 
            index = inf; % GOTO 1102
        end
        index = index + 1;
        if index > npts 
            break;
        end
    end % 1100 while
%1102
    % is the first data point a peak?
    %index = 0;
    if(j == -1)
    %  if( ( j == -1 ) && ( w[0] > tminpeak ) )
        w_pulse( 1:down_index ) = 1;
        index = down_index;
        while true
            w_pulse( index ) = 1; % continue the pulse until the end of the downs
            index = index + 1;
            if( w_downs(index) == -1 )
                break;
            end
        end
        %while( w_downs[ index ] == -1 )
        w_pulse( index ) = 0; % terminate the first pulse!
        %index += 1
        index = index + 1;
        disp(append("pulsetest forced a peak at t=0 through down index: ",num2str(down_index)," index: ", num2str(index)))
        %print "pulsetest forced a peak at t=0 through index",down_index, index
        down_index = index; %- 1
    end
%	doupdate

	%index = down_index-1 % was 0! 20180501 trying to handle first peak
	%do %Loop 1200 of fortran code. Locates ups and sets pulse to true from the location of the up until nPeak points away
    for index= down_index : npts - nPeak + 1 % LOOP 1200
        if(w_ups(index) == 1)
            for j = 0 : nPeak-1 % LOOP 1199
            %do
                w_pulse(index + j) = 1;
            %j += 1
            %while(j < nPeak - 1)
            end
        end
    %	index += 1
    %while(index < npts)
    end
    %	doupdate

	%index = 1 // fixed 20180501
    %index = down_index+1;
  %IGOR do %Loop 1300 of fortran code. Pulse carries over if not down or previously defined as pulse
    for index = down_index + 1 : npts
        if( ( w_pulse( index ) ~= 1 ) && ( w_downs( index ) ~= -1 ) ) % if it's not a pulse and not a down, keep going !
	        %PULSE(I)=PULSE(I-1)
	        w_pulse( index ) = w_pulse( index - 1 );
        end
	  % IGOR index += 1
      % IGOR while(index < npts)
    end
	%IGOR doupdate

	%IGOR variable icur = npts - 2 % was -1, but slides off the back of the array due to 0-indexing
	%IGOR variable izap = 1
	%IGOR icur = npts - 2; % was -1, but slides off the back of the array due to 0-indexing

    %	ICUR=NPTS
	%	IZAP=.TRUE.
    izap = 1;
    %1301    ICUR=ICUR-1 % this is inside the loop, brainiac
    %do  %	IF(ICUR.LT.2) GO TO 1302
    icur = npts-1;
    while true
    %for icur = npts : -1 : nNadir+1 % start at the end, stop before the beginning!
        %	IF(PULSE(ICUR)) IZAP=.FALSE.
        if( w_pulse( icur ) == 1 ) % izap is the opposite of pulse
            izap = 0;
        end
        %	IF(.NOT.IZAP) GO TO 1310
        if( izap == 1 )
            %	PULSE(ICUR)=PULSE(ICUR+1)
            w_pulse( icur ) = w_pulse( icur + 1 );  % here's why we need the -2 at the top of this loop
            %	IF(.NOT.DOWN(ICUR)) GO TO 1301
            if w_downs( icur ) == -1 
	            %	PULSE(ICUR)=.TRUE.
	            %20170109
	            % original: w_pulse[ icur ] = 1
	            % this is the modified code: w_pulse[ icur - 1 ] = 1
	            w_pulse( icur ) = 1;
	        end
            %GO TO 1301
        else % if izap == 0
            %1310    CONTINUE
            %	IF(.NOT.PULSE(ICUR)) GO TO 1301
            if( ( w_pulse( icur ) == 1 )&&( w_pulse( icur-1 ) == 0 ) )
            %	IF(PULSE(ICUR-1)) GO TO 1301
	            izap = 1;
	            %	IZAP=.TRUE.
                icur = icur - nNadir; % matlab
	            %	ICUR=ICUR-NNADIR
            end
        end
        %	GO TO 1301
        %doupdate
        icur = icur - 1;
        %HANDLED IN FOR LOOP DEC icur = icur - 1;
        if icur < 2
            break;
        end
    %while( icur > 2 )
    end % 1302 while!  //end for loop over icur

% NEW HEURISTIC 20170110 :: for some reason, above code extends the pulse through the down,
% sometimes extending the end of a pulse beyond the edge of activity.
%The following code checks the raw data for a zero (no activity) and the pulse wave.
    if( ZeroTerminate == 1 )
        %index = 0
        %do
        for index = 1:npts
            %print "zeroterminate activated in pulsetest", w[index], w_pulse[index]
            %if( ( w[ index ] == 0 ) && ( w_pulse[ index ] == 1 ) )
            if( ( data( index )  <= zero ) && ( w_pulse( index ) == 1 ) )
            %print "zeroterminate activated in pulsetest", w[index], w_pulse[index]
                w_pulse( index ) = 0;
            end
            %index += 1
        %while( index < npts )
        end
    end

	%return wn_pulse
end