function [h] = plotCluster( xdata, data, pulses, ups, downs, tUP, tDN )

pulseColor = [1 0.9 1];

h = figure;
%t=tiledlayout(5,1,'TileSpacing','tight');
hold on

ax1 = subplot(4,1,1); %nexttile; %(t,1,'span',[1 1]);
yyaxis left
plot(xdata, data, '-k');
ylabel('amplitude')
yyaxis right
bar(xdata,pulses,'FaceColor', pulseColor,'EdgeColor','none');
ylabel('pulses');%, 'color', pulseColor)
set(gca, 'SortMethod', 'depth')
xlim tight

ax2 = subplot(4,1,2); %nexttile(t,2);
hold on
color='g';
plot(xdata,ups,'o','Color', color, 'MarkerFaceColor', color );
color='r';
plot(xdata,downs,'o','Color', color, 'MarkerFaceColor', color );
ylabel('UPs and DOWNs')
set(gca, 'SortMethod', 'depth')
xlim tight

ax3 = subplot(4,1,3); %nexttile(t,3);
plot(xdata,tUP,'-r'); %,'Color', color, 'MarkerFaceColor', color );
ylabel('mscore UP')
xlim tight

ax4 = subplot(4,1,4); %nexttile(t,4);
color='r';
plot(xdata,tDN,'-k'); %,'Color', color, 'MarkerFaceColor', color );
ylabel('mscore DN')
set(gca, 'SortMethod', 'depth')
xlim tight

linkaxes( [ax1, ax2, ax3, ax4], 'x')
end