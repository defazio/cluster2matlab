function ms = mScore( ipt, nNadir, nPeak, sdev, w, error ) %//, ndfwn, sdev )
% //////////////////////////////////////////////////////////
% //
% // 						the Notorious M Score
% //
% // a FORTRAN conversion project
% // ~ lord bocaj cloudkiller and papa, summer 2016

% this is supposedly a t-score but it is clearly not related to a t-test
% so we renamed it the "m" score after Mike the author of the FORTRAN code
% ipt is the point tested
% nNadir, nPeak number of points to consider for baseline and peak
% sdev is an array that depends on error param
% w is the array containing data (MEAN in FORTRAN)
% //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

% NDF appears to be the number of degrees of freedom
% we're setting to 1, probably should be the number of samples
% used to get the mean value at each point
%ndf = zeros( size(w) ); % ndf not implemented
ndf = ones( size(w) ); 
% determine stdev array
if max(size(error)) > 1 
    stdev = error; % if error is provided, use it!
else
    stdev = ones(size(w));
    stdev = stdev * sdev; % if error not provided
    % stdev is a constant based on the value sdev
end
% in function UPorDN these are the ranges of ipt
% //   IFIRST=NNADIR+1
% //   ILAST=NPTS-NPEAK+1
% //   DO 9000 IPT=IFIRST,ILAST
% // :: :  :   :    : note the lines above are from original code, now we're running the "mscore" as a function
Pmean = 0.0;
Nmean = 0.0;
sumP = 0;
sumN = 0;
% DO 1500 I=1,NNADIR
for i = 1 : nNadir
    j = ipt - i;
    % // NMEAN=NMEAN+(NDF(J)+1)*MEAN(J)
    Nmean = Nmean + ( ndf(j) + 1 ) * w(j);
    % // SUMN=SUMN+(NDF(J)+1)
    sumN = sumN + (ndf(j)+1);
end % //1500    CONTINUE
Nmean = Nmean / sumN;

%//  DO 1600 I=1,NPEAK
for i = 1 : nPeak
    j = ipt - 1 + i; % ipt - 1 + i;
    %//	PMEAN=PMEAN+(NDF(J)+1)*MEAN(J)
    Pmean = Pmean + (ndf(j) + 1) * w(j); % // you can put mean into w if you want!
    %// SUMP=SUMP+(NDF(J)+1)
    sumP = sumP + (ndf(j)+1);
end  % //1600    CONTINUE
Pmean = Pmean / sumP;

s = 0.0;
izzz1 = ipt - nNadir; % + 1;
izzz2 = ipt + nPeak -1;
%	IZZZ1=IPT-NNADIR
%	IZZZ2=IPT+NPEAK-1
%   DO 1700 I=IZZZ1,IZZZ2
for	i = izzz1 : izzz2
%   S=S+NDF(I)*STDEV(I)**2
%    s = s + ndf(i) * stdev(i)^2;
%    s = s + (ndf(i)+1) * stdev(i);% IGOR code uses this!
    s = s + ndf(i) * stdev(i)^2;
end
%   1700    CONTINUE
%   S=SQRT(S/(SUMN+SUMP-2))
%	T=(PMEAN-NMEAN)/S/SQRT(1./SUMN+1./SUMP)
s = sqrt( s / ( sumN + sumP - 2 ) ); % //original S=SQRT(S/(SUMN+SUMP-2))
Tout = ( Pmean - Nmean ) / s / sqrt( 1/sumN + 1/sumP); %  // / SD				//original	.../S/SQRT(1./SUMN+1./SUMP)

ms = Tout;
%//       IF((T.GT.Z).and.(pmean.gt.dvmp)) UP(IPT)=.TRUE.
%//9000    CONTINUE

end %// mScore