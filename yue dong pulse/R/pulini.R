"pcpker" <-
function(x, y, data, d, nb, rk, limsr, ngrid, grid, method = "GCV", limnla=c(-10, 3),
                  thresh = c(0.9, 0.95, 0.99), ...) {
# Partial Spline Model for detection for sigularites
  #browser()
  title.x <- deparse(substitute(x)) # R-S
  method <- ifelse( method == "GCV", "v", "m")

  thresh <- sort(thresh)
  tmsg <- paste(as.character(thresh * 100), "%", sep = "")
# set up flag single pt/multiple pt
  if ( ! missing(grid) ) {
      grid <- sort(grid)
   # check grid, a priority    
      if ( length(unique(grid)) == 1 ) {
        # check length of grid
        flg.single <- T
        xgrid.old <- unique(grid) }
      else {
        flg.single <- F
        xgrid.old <- grid
        }    
  } else {
# check limsr
      if (! missing(limsr)) {
        limsr <- sort(limsr)
        # check length of limsr
        if (length(unique(limsr))==1) {
          flg.single <- T
          xgrid.old <- unique(limsr)
          }
        else {
          flg.single <- F
          if ( missing(ngrid) ) ngrid <- length(x)
          xgrid.old <- seq( min(limsr), max(limsr) , len=ngrid)
          }
        } else {
          # free style, adj, a tunig parameter to avoid boundary effects.
          adj <- 1
          flg.single <- F
          if (missing(ngrid)) ngrid <- length(x)
          side <- c(1:adj, (ngrid - adj + 1):ngrid)
          xgrid.old <- seq( min(x), max(x) , len=ngrid)[-side]
          }
}
# xgrid.old not scaled & xSF4 only accept for scaled version
  xgrid <- xscale(xgrid.old,x)
  x <- xscale(x) 
  data[,1] <- x
  if  (flg.single) {
    tmp <- xSF4(xgrid, data,  nb=nb, mat=eval(rk, env=data), bar = 0.5 + thresh/2, type = d, method = method, limnla=limnla, ...)
    layout <- c(xgrid.old, tmp)
    layout <- matrix(layout, nrow=1)    
  } else {
    tmp <- matrix(unlist(lapply(as.list(xgrid), FUN = "xSF4", data=data, nb=nb, mat = eval(rk, env=data), bar = 0.5 + thresh/2, type = d, method = method, limnla=limnla, ...), use.names = F), byrow = T, ncol = 3 + length(thresh))
    layout <- cbind(xgrid.old, tmp)
  }
  dimnames(layout) <- list(1:length(xgrid),c(title.x, "coef", "tvalue", "nlaht", tmsg)) #R-S

  pcp.obj <- list(output = layout, x = eval(match.call()$x, sys.parent()),
                  y = y, limsr = if (missing(limsr)) NULL else limsr ,
                  ngrid = if (missing(ngrid)) NULL else ngrid,
                  grid = if (missing(grid)) NULL else grid, 
                  thresh = thresh, method = method, limnla=limnla, 
                  order = d, rk = match.call()$rk,
                  type = switch(as.character(d),"0" = "m","1" = "d","h"))
  class(pcp.obj) <- "pcp"
  return(pcp.obj)  
}  
"print.pcp" <- 
function(xx, complete = FALSE)
{
# print pcp object
  cat("\nPCP for Sigularites. ")
  vm <- ifelse(xx$method == "m", "GML", "GCV")
  cat(paste("(", as.character(xx$rk)[1], "spline", "with", vm, ")","\n\n"))

  extcol <- pmatch(c("coef", "nlaht"), dimnames(xx$output)[[2]])
  msg.d <- "Result : '+1' = significant  pulse\n         '-1' = significant rising\n\n"
  msg.m <- "Result : '+1' = downward jump\n         '-1' = upward jump\n\n"
  # leave 'h' higher order blank
  cat(switch(xx$type,
             d = msg.d,
             m = msg.m,
             h = ""))
  if (dim(xx$output)[1] != 1) {
    if(complete)
      print(xx$output[,  - extcol])
    else {
      # return significance only
      mthresh <- paste(min(xx$thresh) * 100, "%", sep = "")
      tmp <- (xx$output[, mthresh] == 1) | (xx$output[, mthresh] == -1)
      # tmp is logical, test is only one left
      if ( sum(tmp) == 1 )
        print(as.matrix(xx$output[tmp,  ])[,  - extcol]) else
      print(xx$output[tmp,  ][,  - extcol])
      }
    } else print(xx$output)
  invisible()
}
"xscale" <- 
function(x, s = x)
{
# scale a vector ...
  if(max(x) <= max(s)) {
    #x <- as.vector(x)
    (x - min(s))/diff(range(s))
    }
  else stop(paste(deparse(substitute(x)), "should be less than", deparse(substitute(s))))
}
#
"j.ssrformx" <- 
function(lp, res, ind, d) {
# Last modified on  1:03AM Sun Apr 28 2002
# given jumps locations, generate the jump formula in ssr
# y ~ x + sum(jumping terms) for general d
# lp : a vector;
# res and ind: character strings of reasponse and independent
  OLD.options <- options()
  on.exit(options(OLD.options))
  options(width = 500)
  assign("res", res, frame = 1)
  assign("ind", ind, frame = 1)
  assign("d", d, frame = 1)
  seg <- apply(as.matrix(lp), 1, FUN = function(x)
               {
                 tmp <- Split(paste("I((", ind, "-P)^", d, "*(", ind, ">P))", sep = ""), "P")
                 tmpx <- paste(c(substring(tmp[1], 1, nchar(tmp[1]) - 1),
                                 substring(tmp[2], 1, nchar(tmp[2]) - 1)), x)
                 paste(tmpx[1], tmpx[2], tmp[3], collapse = "")
                 }
               )	# end apply(...
  claps <- paste(seg, "+", collapse = "")
  claps <- paste(paste(res, "~", ind, "+", sep = ""), substring(claps, 1, nchar(claps) - 1), sep = "")
  return(formula(claps))
}
"xSF4" <- function(k, data, nb,  mat, bar = c(0.90,0.95, 0.99), type = 0, method = "m", limnla=c(-10, 3), ...) {
# only internal use
  old.opt <- options()
  on.exit(options(old.opt))
  options(contrasts=c("contr.treatment","contr.poly"))
  ind <- dimnames(data)[[2]][1]
  res <- dimnames(data)[[2]][2]
  varName <- dimnames(data)[[2]][3]
  form <- paste(res, deparse(nb), "+I( ( (", varName, "-k)^", type, " )*(", varName, "> k) )", sep = "")
  # R&S or get("mat",pos=sys.frame(0))
  assign("mat",mat,envir=sys.frame(0))
  fit <- dsidr(y=data[, res], q=mat, s=model.matrix(as.formula(form), data=data), vmu=method, limnla=limnla)
  
#  fit <- ssr(formula = as.formula(form), rk = mat,
#             data = data, method = method,limnla=limnla, ...)
##################################
Call <- quote(ssr(formula = as.formula(form), rk = list(mat),
             data = data, method = method,limnla=limnla))
result <- list(call = Call, expand.call = Call, data = data, 
        y = data[, res], weight = NULL, fit = as.vector(fit$fit), 
        s = model.matrix(as.formula(form), data=data), q = list(mat), lambda = 10^fit$nlaht/dim(data)[1], residuals = as.vector(fit$resi), 
        sigma = sqrt(fit$varht), family = "gaussian", coef = list(c = fit$c, 
            d = fit$d), df = fit$df, rkpk.obj = fit, 
        scale = FALSE, cor.est = NULL, var.est = NULL, 
        control = ssr.control())
   
    class(result) <- "ssr"
    result$call[[1]] <- as.name("ssr")
   
    result$lme.obj <- NULL

#############################
  nterms <- length(attr(terms(as.formula(form)), "order"))
  xx <- predict(result, terms=c(rep(0, nterms), 1,0))

#########################
#  est <- unique(xx$fit[xx$fit!=0]) # toubles here
#  est.sd <- unique(xx$pstd[xx$pstd!=0])
#  tvalue <- est/est.sd
#  print(length(tvalue))
################################
  est <- xx$fit[xx$fit!=0]
  est.sd <- xx$pstd[xx$pstd!=0]
  tvalue <- (est/est.sd)[1]  
################################
#bar > 0.5 so thresh guaranteed positive
  thresh <- apply(as.matrix(bar), 1, qt, df = dim(data)[1] - length(fit$d) )
  sgn <- ifelse(tvalue < 0, 1, -1) #R-S
  remove(mat, envir=sys.frame(0))
  return( c(fit$d[3], tvalue, fit$nlaht, sgn*as.numeric(abs(tvalue) >= thresh)) )
}

"whereIs.cluster" <- function(y) {
 # GIVEN A SEQUENCE OF Y WHICH ARE THE VALUES OF Y GREATER THAN THE THRESHOLD; ZERO OTHERWISE.
 # RETURN THE INDEX OF INTERVALS OF CLUSTERS
  y <- c(y,0) # control RHS end-point
  tmp <- TRUE
  n <- length(y)
  idx <- numeric(0)
  for ( i in 1:n ) {
    if (tmp) {
      if ( y[i] == 1 ) {
        idx <- c(idx,i)
        tmp <- FALSE
        }
      } else {
        if ( y[i] != 1 ) {
          idx <- c(idx,i-1)
          tmp <- TRUE
          }
        }
    }
  return(idx)
}
#############################################################################

##################################################
pcp <- function(x, y, data=sys.parent(), d=1, spline = list(nb=~x, rk=cubic(x)), spar="v", limnla=c(-10, 3), alpha)
{
     if(alpha > 1 | alpha < 0) 
         stop("alpha must be in [0,1]")
     xcall <- match.call()
     res <- deparse(xcall$y)
     ind <- deparse(xcall$x)
     data.orig <- data
     x <- eval(xcall$x, data.orig)
     y <- eval(xcall$y, data.orig)
     nb <- ~x
     rk <- quote(cubic(x))
     if(is.null(xcall$spline$nb) != is.null(xcall$spline$rk) )
          stop("for spline smoothing, nb and rk have to specified at the same time with the same covariates")
     varName <- "x"       
     if(!is.null(xcall$spline))
           { 
            if(is.element("list", all.names(xcall$spline)))
                   {nb <- xcall$spline$nb
                    rk <- xcall$spline$rk
##temporary solution for different inputs of rk
                    varName <- unique(all.vars(rk))
                   funName <- unique(all.names(rk))
                  if(length(varName) !=1 | all(is.element(funName,varName)))
                    { rk <- eval(rk, sys.parent())
                      varName <- unique(all.vars(rk))
                      funName <- unique(all.names(rk))
                      if(length(varName) !=1 | all(is.element(funName,varName)))
                            stop("wrong format of the rk input")
                      }
                   nb <- eval(nb, sys.parent())
                   }
             else
                {spline <- eval(xcall$spline,sys.parent())
                 nb <- spline$nb
                 rk  <- spline$rk
                 varName <- all.vars(nb)
                }
            }
         
     tt <- eval(as.name(varName), env=data.orig)
     data <- data.frame(xscale(x), y, xscale(tt))
     dimnames(data)[[2]] <- c(ind, res, varName)
     

thresh <- 1-alpha
fit.pcp <- pcpker(x,y, data, d = d, nb=nb,  rk=rk, method= spar, limnla=limnla, thresh=thresh)
# pick up the optimum of clusters
     # use whereIs.cluster() to find the index of clusters
        xx <- fit.pcp$output
        X <- xx[,"x"]
        mthresh <- paste(min(thresh) * 100, "%", sep = "")
        Y <- xx[,mthresh] 
        xx <- xx[,c("x", "tvalue")] 
        yy <- matrix(whereIs.cluster(Y), ncol=2, byrow=T)
        init.cp <- apply( yy, 1,
                         FUN=function(x, ...)
                         {
                           cluster.t <- xx[x[1]:x[2], "tvalue"]
                           pulse.t <- cluster.t[cluster.t==min(cluster.t) & cluster.t < 0]
                           loc <- (1:length(cluster.t))[cluster.t==pulse.t]
                           temp <- xx[x[1]+loc-1,1]
                           if (length(temp) != 1) temp <- mean(temp)
                           return(temp)
                           }, xx )
       init.cp <- init.cp[is.finite(init.cp)]
     # end of whereIs.cluster and return the optimal values for each cluster as vector init.cp   
}
##################################################

##################################################
# xx <- pcp(x,y, d = 1, rk=cubic,method= "m")
#  extcol <- pmatch(c("coef", "nlaht"), dimnames(xx$output)[[2]])
#     mthresh <- paste(min(xx$thresh) * 100, "%", sep = "")
#      tmp <- (xx$output[, mthresh] == 1) | (xx$output[, mthresh] == -1)
#      # tmp is logical, test is only one left
#      if ( sum(tmp) == 1 )
#        loc <- as.matrix(xx$output[tmp,  ])[,  - extcol] else
#      loc <- xx$output[tmp,  ][,  - extcol]
#locts <- loc[loc[,"80%"]==1,"x"]
#tmp <- sapply(locts, function(x) lines(c(x,x), c(0, 2)))
##################################################

CLUSTER <- function(x, y, data=sys.parent(), sd=mean(y)*0.07, nnadir=2, npeak=3, alpha)
{ xcall <- match.call()
  x <- eval(xcall$x, data)
  y <- eval(xcall$y, data)
  n <- length(y)
  p1 <- p2 <- rep(NA, n)
  if(nnadir > npeak) dest <- nnadir-1
  else dest <- npeak
  for(i in 1:(n-dest))
   {s1 <- y[i: (i+nnadir-1)]
    s2 <- y[(i+1):(i+npeak)]
    p1[i] <- 1-pnorm((mean(s2)-mean(s1))/sd/sqrt(1/sqrt(nnadir)+1/sqrt(npeak)))
    s1 <- y[i: (i+npeak-1)]
    s2 <- y[(i+1):(i+nnadir)]    
    p2[i] <- pnorm((mean(s2)-mean(s1))/sd/sqrt(1/sqrt(nnadir)+1/sqrt(npeak)))
   }
   if(dest > 2)
   {
   for(i in 1:(dest-2))
   { s1 <- y[(n-dest+i):(n-dest+i+1)]
     s2 <- y[(n-dest+i+1):(n-dest+i+2)]
     p1[n-dest+i] <- 1-pnorm((mean(s2)-mean(s1))/sd/sqrt(1/sqrt(nnadir)+1/sqrt(npeak)))
     p2[n-dest+i] <- pnorm((mean(s2)-mean(s1))/sd/sqrt(1/sqrt(nnadir)+1/sqrt(npeak)))
   }
    {p1[n-1] <- !(y[n] > y[n-1])
     p2[n-1] <- !(y[n] < y[n-1]) 
    }}
#   plot(x,y, type="b", pch=20, ylim=c(0, max(y)+0.5))
   tmp <- x[p1 < alpha]
#   points(tmp, rep(0, length(tmp)), col=2, pch=1)
   tmp1 <- x[p2 < alpha]
#   points(tmp1, rep(0, length(tmp1)), col=4, pch=2)
   sequ <- c(rep(1, n)[(p1 < alpha)&!is.na(p1)], rep(0,n)[(p2 < alpha)&!is.na(p2)])
   p.sequ <- c(p1[(p1 < alpha)&!is.na(p1)], p2[(p2 < alpha)&!is.na(p2)])
   x.sequ <- c(x[(p1 < alpha)&!is.na(p1)], x[(p2 < alpha)&!is.na(p2)])
   y.sequ <- c(y[(p1 < alpha)&!is.na(p1)], y[(p2 < alpha)&!is.na(p2)])
   sequ <- sequ[order(x.sequ)]
   y.sequ <- y.sequ[order(x.sequ)]
   x.sequ <- x.sequ[order(x.sequ)]
   lp <- lp.p <- NULL
   if(length(sequ) < 3)
     return(x.sequ(sequ==0))
   else
   {
     for(i in seq(length(sequ)-2))
     {if((sequ[i] ==1) & (sequ[i+1]==0)) 
     {count <- 1 
     for(j in (i+2):length(sequ))
     {
     if(sequ[j]==0) count <- count+1
     if(sequ[j]==1) break
     }
     tmp <- x.sequ[(i+1):(i+count)]
     tmp.p <- p.sequ[(i+1):(i+count)]
     tmp1 <- tmp[y.sequ[(i+1):(i+count)]==max(y.sequ[(i+1):(i+count)])]
     tmp1.p <- tmp.p[y.sequ[(i+1):(i+count)]==max(y.sequ[(i+1):(i+count)])]
     lp <- c(lp, tmp1[1])
     lp.p <- c(lp.p, tmp1.p[1])
     }}
     return(lp)
   }}

pulini <- function(x, y, data=sys.parent(), method=c("pcp", "CLUSTER"), alpha, control=list(pcp=list(spline=list(nb=~x, rk=cubic(x)), spar="v", limnla=c(-10, 3)), cluster=list(sd=mean(y)*0.07, nnadir=2, npeak=3)))
   { 

     if(alpha < 0 | alpha > 1)
          stop("alpha must in [0,1]")

     xcall <- match.call()
     res <- deparse(xcall$y)
     ind <- deparse(xcall$x)
     data.orig <- data
     x <- eval(xcall$x, data.orig)
     y <- eval(xcall$y, data.orig)
    ctr <- list(pcp=list(spline=list(nb=~x, rk=quote(cubic(x))), spar="v", limnla=c(-10, 3)), cluster=list(sd=mean(y)*0.07, nnadir=2, npeak=3))

    if(!is.null(xcall$control))
    {
        if(is.null(xcall$control$pcp) & is.null(xcall$control$cluster)) stop("control argument must be a list with one of the names: pcp and cluster")
        if( is.null(xcall$control$pcp$spline$nb) != is.null(xcall$control$pcp$spline$rk) )
          stop("for spline smoothing, nb and rk have to specified at the same time with the same covariates")
            
        if(!is.null(xcall$control$pcp))
           ctr$pcp[names(xcall$control$pcp)] <- xcall$control$pcp
        if(!is.null(xcall$control$cluster))
           ctr$cluster[names(xcall$control$cluster)] <- xcall$control$cluster
    }
    
       
     data <- data.frame(x, y, x)
     varName <- "x"
     dimnames(data)[[2]] <- c(ind, res, varName)
   
     rkCall <- ctr$pcp$spline$rk
     tt <- x

     if(!is.null(xcall$control$pcp$spline$nb))
     {
            rkCall <- xcall$control$pcp$spline$rk
            varName <- unique(all.vars(rkCall))
            tt <- eval(as.name(varName), env=data.orig)
            data <- data.frame(x, y, tt)
            dimnames(data)[[2]] <- c(ind, res, varName)
      }
     
    if(method=="pcp") lp <- pcp(x, y, data=data, d=1, spline=list(nb=ctr$pcp$spline$nb, rk=ctr$pcp$spline$rk), spar=eval(ctr$pcp$spar), limnla=eval(ctr$pcp$limnla), alpha=alpha)
   if(method=="CLUSTER") lp <- CLUSTER(x, y, data=data, sd=eval(ctr$cluster$sd), nnadir=eval(ctr$cluster$nnadir), npeak=eval(ctr$cluster$npeak), alpha)
   return(lp)}

#pulini.control <- function(nb=~x, rk=cubic(x), spar="v", limnla=c(-10, 3), sd=0.1, nnadir=2, npeak=3)
#                  list(pcp=list(spline=list(nb=nb, rk=rk), spar=spar, limnla=limnla), cluster=list(sd=sd, nnadir=nnadir, npeak=npeak))

ssr.control <- 
function (job = -1, tol = 0, init = 0, theta = NULL, prec = 1e-06, 
    maxit = 30, tol.g = 0, prec.g = 1e-06, maxit.g = 30) 
{
    if ((init == 1) && is.null(theta)) 
        stop("initial values for theta is needed")
    list(job = job, tol = tol, init = init, theta = theta, prec = prec, 
        maxit = maxit, tol.g = tol.g, prec.g = prec.g, maxit.g = maxit.g)
}

 

