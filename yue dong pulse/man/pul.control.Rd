\name{pul.control}
\alias{pul.control}
\title{Control Values for puldet and pulest}
\description{The values supplied in the function call replace the 
defaults and a list with all possible arguments is returned. The 
returned list is used as the control argument to the puldet or 
pulestthe function.}
\usage{pul.control(maxIter= 100, pnlsMaxIter=5, msMaxIter=10, 
         tolerance=10e-3, pnlsTol=10e-3, msTol=10e-3, gradHess=TRUE, 
         apVar=TRUE, returnObject=TRUE, msVerbose=FALSE, spar="v", 
         TOLr=10e-3, nlsMaxIter=5, minScale=0.01, nlsTol=0.01, 
         limnla=c(-3,1), IDF=1.2)
} 
\arguments{
\item{spar}{method for selecting the smoothing parameter in the 
estimation of the baseline function. \option{spar="v"} for GCV (default) 
and \option{spar="m"} for GML.} 
\item{limnla}{a vector of length one or two, specifying a search range 
for log10(n*lambda), where lambda is the smoothing  parameter and
n is the sample size. If it is a single value, the smoothing
parameter will be fixed at this value. Default is c(-10, 3).}
\item{TOLr}{tolerance level of the GCV or GML scores for the iteration 
between the baseline estimation and estimation of the pulsatile part.}
\item{IDF}{Inflated degree of freedom due to the selection process.
Default is 1.2.}
\item{trace}{a logic value which indicates whether the selection 
procedure should be printed out.}
\item{others}{Other arguments are used for either \code{\link{nlme}} 
or \code{\link{gnls}} control. Refer to the manual of \code{\link{nlme}}
and \code{\link{gnls}} in the \pkg{NLME} package for their definitions.}
}
\value{a list with names \var{ssr}, \var{pul}, \var{gnls}, and \var{nlme}. 
\var{ssr} is a list with names \var{method} and \var{limnla}. \var{pul}
is a list with names \var{TOLr}, \var{IDF} and \var{trace}. \var{gnls}
and \var{nlme} contains control parameters for the \code{\link{gnls}} 
and \code{\link{nlme}} functions.}
\references{
    Pinheiro, J. and Bates, D. M. (2000), \emph{Mixed-effects Models in 
    S and S-plus}, Springer, New York.

    Yang, Y. (2002), \emph{Detecting Change Points and Hormone Pulses 
    Using Partial Spline Models}, Ph.D. Thesis, University of 
    California-Santa Barbara, Dept. of Statistics and Applied Probability.

    Yang, Y. and Liu, A. and Wang, Y., (2004), \emph{Detecting 
    Pulsatile Hormone Secretions Using Nonlinear Mixed Effects 
    Partial Spline Models}. 
    Available at \url{www.pstat.ucsb.edu/faculty/yuedong/research}.

    Wang, Y. and Ke, C. (2002), \emph{ASSIST: A Suite of S-plus 
    functions Implementing Spline smoothing Techniques}. Available at 
    \url{cran.us.r-project.org/src/contrib/PACKAGES.html}. 
    Manual for the ASSIST package is available at 
    \url{www.pstat.ucsb.edu/faculty/yuedong/software}.
}
\author{Yu-Chieh Yang, Anna Liu, Yuedong Wang}
\seealso{\code{\link{gnls}}, \code{\link{nlme}}, \code{\link{puldet}}, 
\code{\link{pulest}, \code{\link{ssr}}}
\examples{
# change IDF=1 when there is no selection and
# decrease the maximum number iterations    
pul.control(maxIter= 30, IDF=1.2)
}
