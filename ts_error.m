% ////////////
% //
% // ts_error
% // 20180425 td added optional param to pass error wavename
% ////////////
% 20230214 porting to matlab
function err = ts_error( w, errorType, errorValue, nPeak, nNadir )  %//20170109 uses duplicate already !!
% w is vector containing data
% errorType is string defined below
% errorValue is default error if error cannot be calculated, usually 0?
% nPeak
% nNadir
% errData user supplied error data

n = max( size(w) );
err = zeros( size(w) );

switch errorType
    case "SQRT"

        for i = 0 : n 
            if w(i) > 0
                err(i) = sqrt( w(i) );
            else
                err(i) = errorValue;
            end
        end

	case "Fixed"

		err = err + errorValue;

	case "Global SD"

		err = err + std( w, 'omitnan' ); % V_sdev

	case "Global SE"
	
        err = err + std(w) / sqrt( length(w) ); % V_sem

	case "Local SD"

        err = movstd(w,[nNadir,nPeak]);

	case "Local SE"

        err = movstd(w,[nNadir,nPeak])/sqrt(nNadir+nPeak);

    otherwise
        warning('unexpected error type in tserror function')

end

end