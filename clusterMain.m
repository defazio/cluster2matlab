function [UPs, DNs, pulse, tsUP, tsDN ] = clusterMain( data, nPeak, nNadir, tScoreUp, tScoreDn, minPeak, HalfLife, outScore, errType, errVal, zero) %, zeroTerminate, errwn, minnadir )
% 	string wn
% 	variable nPeak, nNadir, tScoreUp, tScoreDn, minPeak, halfLife, outScore
% 	string errType
% 	variable errVal
% 	variable zero, zeroTerminate // activate zero bin termiantion of the pulse
%   string errwn
%   variable minnadir

    err = ts_error( data, errType, errVal, nPeak, nNadir );

	[ UPs, tsUP, up_pMeans, up_nMeans ] = UPorDN( data, err, nPeak, nNadir, tScoreUp, 1, minPeak);

	[ DNs, tsDN, dn_pMeans, dn_nMeans ] = UPorDN( data, err, nPeak, nNadir, tScoreDn, -1, minPeak);

    ZeroTerminate = 0;
	pulse = pulseTest( UPs, DNs, nPeak, nNadir, ZeroTerminate, zero ); % , minPeak, minNadir )
    % (data, nPeak, nNadir, ZeroTerminate, zero ); % , minpeak, minnadir );

end
